package Serialize;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import model.Bank;

public class SerializeBank {

	public static void serializeInfo(Bank bank){
		try {
			FileOutputStream fout = new FileOutputStream("bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fout);
			out.writeObject(bank);
			out.close();
			fout.close();
			System.out.println("Object serialized");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
