package Serialize;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import model.Bank;

public class DesiarilizeBank {
	
	public static Bank deserializeInfo(){
		Bank bank ;
		
		bank = null;
		FileInputStream fileIn = null;
		ObjectInputStream in = null;
		try {
			fileIn = new FileInputStream("bank.ser");
			in = new ObjectInputStream(fileIn);
			bank = (Bank)in.readObject();
			System.out.println("Bank deserialized!");
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			System.out.println("Bank class not found");
			c.printStackTrace();
			return null;
		} finally {
			try{
				in.close();
				fileIn.close();

			}catch(IOException e){
				e.printStackTrace();
			}
		}
		
		return bank;
	}
	
}
