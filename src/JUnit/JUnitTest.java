package JUnit;

import static org.junit.Assert.*;
import org.junit.*;


import model.*;

public class JUnitTest {

	private static Bank bank;
	private static Person p1;
	private static Person p2;
	private static SavingAccount c1;
	private static SavingAccount c2;
	private static SpendingAccount c3;

	public JUnitTest() {
		System.out.println("Constructor inaintea fiecarui test!");
	}

	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("O singura data inaintea executiei setului de teste din clasa!");
		p1 = new Person("Gabi","mat.gabi@yahoo.com",21);
		p2 = new Person("Tudor","tudi@yahoo.com",21);
		c1 = new SavingAccount(p1,"123 4125 121",new Float(10000),new Float(0.1));
		c2 = new SavingAccount(p1,"100 0000 000",new Float(13000),new Float(0.1));
		c3 = new SpendingAccount(p2,"200 0000 000",new Float(10000));
		
		bank = new Bank();
		
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("O singura data dupa terminarea executiei setului de teste din clasa!");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Incepe un nou test!");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("S-a terminat testul curent!");
	}

	@Test
	public void addPerson1() {
		bank.addPerson(p1);
	}
	
	@Test
	public void addPerson2() {
		bank.addPerson(p2);
	}

	@Test
	public void addAccounts1() {
		bank.addAccount(p1, c1);
	}
	
	@Test
	public void addAccounts2() {
		bank.addAccount(p1, c2);
	}
	
	@Test
	public void addAccounts3() {
		bank.addAccount(p2, c3);
	}

	@Test
	public void makeWithdraw1(){
		bank.makeWithdraw(c1, 100);
	}
	
	@Test
	public void makeWithdraw2(){
		bank.makeWithdraw(c2, 100);
	}
	
	@Test
	public void makeWithdraw3(){
		bank.makeWithdraw(c3, 100);
	}
	
	@Test
	public void makeWithdraw4(){
		bank.makeWithdraw(c1, 1233);
	}
	
	@Test
	public void makeWithdraw5(){
		bank.makeWithdraw(c2, 1009);
	}
	
	@Test
	public void makeWithdraw6(){
		bank.makeWithdraw(c3, 7658);
	}
	
	@Test
	public void makeDeposit1(){
		bank.makeDeposit(c1, 2314);
	}
	
	@Test
	public void makeDeposit2(){
		bank.makeDeposit(c2, 2314);
	}
	
	@Test
	public void makeDeposit3(){
		bank.makeDeposit(c3, 2314);
	}
	
	@Test
	public void makeDeposit4(){
		bank.makeDeposit(c1, 23140);
	}
	
	@Test
	public void makeDeposit5(){
		bank.makeDeposit(c2, 19983);
	}
	
	@Test
	public void makeDeposit6(){
		bank.makeDeposit(c3, 14255);
	}
	
	@Test
	public void removeAccount1(){
		bank.removeAccount(p1, c1);
	}
	
	@Test
	public void removeAccount2(){
		bank.removeAccount(p1, c2);
	}
	
	@Test
	public void removePerson1(){
		bank.removePerson(p1);
	}
	
	@Test
	public void removePerson2(){
		bank.removePerson(p2);
	}
	
}
