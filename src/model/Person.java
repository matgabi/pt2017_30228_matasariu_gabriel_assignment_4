package model;
import java.util.*;
import java.io.*;

public class Person implements Observer,Serializable {

	private String name;
	private String email;
	private int age;
	private List<Account> accounts;
	
	public Person(String name,String email,int age){
		this.name = name;
		this.email = email;
		this.age = age;
		accounts = new ArrayList<Account>();
	}
	
	public void addAccount(Account ac){
		this.accounts.add(ac);
		this.accounts.get(this.accounts.size() - 1).addObserver(this);
	}
	
	public void update(Observable obs,Object obj){
		for(Account a : accounts ){
			if(a == obs){
				System.out.println("New action on " + this.name + "'s accounts");
				System.out.println("Account id: " + a.getId());
				if(a instanceof SavingAccount){
					System.out.println("Account type: saving account" );
				}
				else
					System.out.println("Account type: spending account" );
				String[] info = ((String)obj).split(",");
				System.out.println("Action type: " + info[1]);	
				Float amount = new Float(info[0]);
				Float init = new Float(info[2]);
				System.out.println("Initial sold: " + init );
				System.out.println("Current sold: " + a.getSold());
				System.out.println("Debit: " + ( a.getSold() - init ));
				
				break;
				
			}
		}
		System.out.println();
		System.out.println();
		
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public int getAge(){
		return this.age;
	}
	public List<Account> getAccounts(){
		return this.accounts;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public void setAge(int age){
		this.age = age;
	}
}
