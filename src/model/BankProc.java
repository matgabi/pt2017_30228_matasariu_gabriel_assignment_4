package model;

import java.util.*;

public interface BankProc {

	/**
	 * @pre p!=null
	 * @post nrOfPersons = @pre nrOfPersons + 1
	 * @invariant isWellFormed()
	 * */
	public void addPerson(Person p);
	
	/**
	 * @pre p!=null
	 * @post nrOfPersons = @pre nrOfPersons - 1
	 * @invariant isWellFormed()
	 * */
	public void removePerson(Person p);
	
	/**
	 * @pre p!=null
	 * @pre c!=null
	 * @post nrOfAccounts = @pre nrOfAccounts + 1
	 * @invariant isWellFormed()
	 * */
	public void addAccount(Person p, Account c);
	
	/**
	 * @pre p!=null
	 * @pre c!=null
	 * @post nrOfAccounts = @pre nrOfAccounts - 1
	 * @invariant isWellFormed()
	 * */
	public void removeAccount(Person p, Account c);
	
	
	/**
	 * @pre c!=null
	 * @pre amount > 0
	 * @post getSold() < @pre getSold()
	 * @invariant isWellFormed()
	 * */
	public void makeWithdraw(Account c,float amount);
	
	/**
	 * @pre c!=null
	 * @pre amount > 0
	 * @post getSold() > @pre getSold()
	 * @invariant isWellFormed()
	 * */
	public void makeDeposit(Account c, float amount);
	public HashMap<Person,List<Account>> getInfo();
	
}
