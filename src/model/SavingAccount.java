package model;

public class SavingAccount extends Account {

	private float interest;
	
	public SavingAccount(Person p, String id,float sold, float interest){
		this.owner = p;
		this.id = id;
		//owner.addAccount(this);
		this.sold = sold;
		this.interest = interest;
	}
	
	public void deposit(float amount){
		float initialSold = sold;
		sold = sold + amount - amount*interest;
		setChanged();
		notifyObservers(amount+"," + "deposit," + initialSold);
	}
	
	public void withdraw(float amount){
		float initialSold = sold;
		this.sold -= (amount + amount * interest);
		setChanged();
		notifyObservers(-amount+","+ "withdraw ," + initialSold);
	}
	
	public float getInterest(){
		return this.interest;
	}
	
	public void setInterest(float interest){
		this.interest = interest;
	}
	
	
}
