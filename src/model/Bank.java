package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Serialize.SerializeBank;

import java.io.*;

public class Bank implements BankProc,Serializable {

	private HashMap<Person,List<Account>> persons;
	private int nrOfPersons;
	private int nrOfAccounts;

	
	public Bank(){
		persons = new HashMap<Person,List<Account>>();

		nrOfPersons = 0;
		nrOfAccounts = 0;
	}
	
	public void addPerson(Person p){
		assert p != null : "Persoana trebuie sa fie nenula!";
		int oldP = nrOfPersons;
		persons.put(p, p.getAccounts());
		nrOfPersons++;
		
		assert nrOfPersons == oldP + 1 : "Eroare aparuta la introducere persoana";
		assert isWellFormed();
		SerializeBank.serializeInfo(this);
	}
	
	
	public void removePerson(Person p){
		assert p != null : "Persoana trebuie sa fie nenula!";
		int oldP = nrOfPersons;

		persons.remove(p);
		nrOfPersons--;
		
		assert nrOfPersons == oldP - 1 : "Eroare aparuta la stergere persoana";
		assert isWellFormed();
		SerializeBank.serializeInfo(this);
	}
	
	public void addAccount(Person p, Account c){
		int oldA = nrOfAccounts;
		
		assert p != null : "Persoana null";
		assert c != null : "Cont null";
		
		p.addAccount(c);
		nrOfAccounts++;
		
		assert nrOfAccounts == oldA  + 1 : "Eroare apparuta la introducere cont!";
		assert isWellFormed();
		SerializeBank.serializeInfo(this);
	}
	public void removeAccount(Person p, Account c){
		int oldA = nrOfAccounts;
		
		assert p != null : "Persoana null";
		assert c != null : "Cont null";
		
		p.getAccounts().remove(c);
		nrOfAccounts--;

		assert nrOfAccounts == oldA  - 1 : "Eroare apparuta la stergere cont!";
		assert isWellFormed();
		
		SerializeBank.serializeInfo(this);
	}
	
	public void makeWithdraw(Account c,float amount){
		assert c != null : "Cont null";
		assert amount > 0 : "Suma incorecta";
		float oldS = c.getSold();
		
		c.withdraw(amount);
		
		assert c.getSold() < oldS : "Retragere incorecta";
		assert isWellFormed();
		SerializeBank.serializeInfo(this);
	}
	
	public void makeDeposit(Account c, float amount){
		assert c != null : "Cont null";
		assert amount > 0 : "Suma incorecta";
		float oldS = c.getSold();
		
		c.deposit(amount);
		
		assert c.getSold() > oldS : "Depunere incorecta";
		assert isWellFormed();
		
		SerializeBank.serializeInfo(this);
	}
	
	public HashMap<Person,List<Account>> getInfo(){
		return persons;
	}
	
	
	public List<Person> getPersons(){
		Set set = this.getInfo().entrySet();
		Iterator it = set.iterator();
		List<Person> persons_ = new ArrayList<Person>();
		while(it.hasNext()){
			Map.Entry mentry = (Map.Entry)it.next();
			Person p = (Person)mentry.getKey();
			persons_.add(p);
		}
		
		return persons_;
	}
	
	
	public List<TableAccount> getAccounts(){
		Set set = this.getInfo().entrySet();
		Iterator it = set.iterator();
		List<TableAccount> accounts  = new ArrayList<TableAccount>();
		while(it.hasNext()){
			Map.Entry mentry = (Map.Entry)it.next();
			Person p = (Person)mentry.getKey();
			List<Account> ac = p.getAccounts();
			for(Account c : ac){
				String o = c.getOwner();
				String id = c.getId();
				float s = c.getSold();
				float i = c.getInterest();
				String t = null;
				if(c instanceof SavingAccount){
					t = "saving account";
				}else{
					t = "spending account";
				}
				accounts.add(new TableAccount(o,id,s,i,t));
			}
		}
		return accounts;
	}

	private boolean isWellFormed(){
		Set set = this.getInfo().entrySet();
		Iterator it = set.iterator();
		while(it.hasNext()){
			Map.Entry mentry = (Map.Entry)it.next();
			Person p = (Person)mentry.getKey();
			if(p == null || p.getAccounts() == null)
				return false;
		}
		return true;
	}
}
