package model;
import java.io.*;
import java.util.*;
import model.*;
import Serialize.DesiarilizeBank;
import Serialize.SerializeBank;

public class Test2 {

	public static void main(String[] args) {
		
		/*
		Bank bank = DesiarilizeBank.deserializeInfo();
		
	
		Set set = bank.getInfo().entrySet();
		Iterator it = set.iterator();
		
		while(it.hasNext()){
			Map.Entry mentry = (Map.Entry)it.next();
			Person per = (Person)mentry.getKey();
			System.out.println(per.getName());
			List<Account> acounts = (List<Account>)mentry.getValue();
			for(Account c : acounts){
				System.out.println(c.getId());
			}
				
		}*/
		
		//// aici pot schimba continutul
		
		Person p = new Person("Gabi","mat.gabi@yahoo.com",21);
		Person p1 = new Person("Tudor","tudi@yahoo.com",21);
		SavingAccount s1 = new SavingAccount(p,"123 4125 121",new Float(129),new Float(0.1));
		SavingAccount s2 = new SavingAccount(p,"100 0000 000",new Float(130),new Float(0.1));
		SpendingAccount s3 = new SpendingAccount(p1,"200 0000 000",new Float(1000));
		
		Bank bank = new Bank();
		bank.addPerson(p1);
		bank.addPerson(p);
		bank.addAccount(p1, s3);
		bank.addAccount(p, s1);
		bank.addAccount(p, s2);
		
		
		SerializeBank.serializeInfo(bank);
		
		
		
	}
	
}
