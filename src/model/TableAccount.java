package model;

public class TableAccount {

	private String owner;
	private String id;
	private float sold;
	private float interest;
	private String type;
	
	public TableAccount(String owner,String id,float sold, float interest,String type){
		this.owner = owner;
		this.id = id;
		this.sold = sold;
		this.interest = interest;
		this.type = type;
	}
	
}
