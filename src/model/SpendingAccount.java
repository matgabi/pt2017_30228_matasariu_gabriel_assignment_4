package model;

public class SpendingAccount extends Account {

	public SpendingAccount(Person p,String id,float sold){
		this.owner = p;
		//owner.addAccount(this);
		this.id = id;
		this.sold = sold;
	}
	
	public float getInterest(){
		return 0;
	}
	
}
