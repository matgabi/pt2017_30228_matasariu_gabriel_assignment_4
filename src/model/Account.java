package model;
import java.util.Observable;
import java.io.*;

public class Account extends Observable implements Serializable {

	protected Person owner;
	protected String id;
	protected float sold;
	
	public void deposit(float amount){
		float initialSold = sold;
		sold = sold + amount;
		setChanged();
		notifyObservers(amount+"," + "deposit," + initialSold);
	}
	
	public void withdraw(float amount){
		float initialSold = sold;
		this.sold -= amount;
		setChanged();
		notifyObservers(-amount+","+ "withdraw, " +initialSold);
	}
	
	public String getId(){
		return this.id;
	}
	
	public float getSold(){
		return sold;
	}
	
	public String getOwner(){
		return this.owner.getName();
	}
	
	public void setOwner(Person p){
		this.owner = p;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public void setSold(float sold){
		this.sold = sold;
	}
	
	public String toString(){
		return this.id;
	}
	
	public float getInterest(){
		return 0;
	}
	
	
	
}
