package presentation;

import java.lang.reflect.Field;

import javax.swing.*;
import java.util.*;
import java.awt.Color;
import java.awt.Component;

import javax.swing.table.*;


public class ReflectionTable {
	
	private static int fieldLength;
	private static String[] fields;
	private static String[][] content;
	
	private static void retrieveFields(Object obj){
		Field[] field = obj.getClass().getDeclaredFields();
		fields = new String[field.length];
		fieldLength = field.length;
		for(int i = 0 ; i < field.length ; i++){
			fields[i] = field[i].getName();
			//System.out.println(field[i].getName());
		}
	}
	
	private static String[] retrieveContent(Object obj){
		String[] contentLine = new String[fieldLength];
		Field[] field = obj.getClass().getDeclaredFields();
		for(int i = 0 ; i < field.length ; i++){
			field[i].setAccessible(true); // set modifier to public
			Object value;
			try {
				value = field[i].get(obj);
				contentLine[i] = value.toString();
				//System.out.println(value.toString());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} 
		}
		return contentLine;
	}
	
	public static JTable retrieveTable(List<? extends Object> obj){
		if(obj.size() != 0){
			retrieveFields(obj.get(0));
			content = new String[obj.size() + 1][fieldLength];
			content[0] = fields;
			int i = 1;
			for(Object o : obj){
				content[i++] = retrieveContent(o);
			}
			JTable table = new JTable(content,fields);
			return table;
		}
		return null;
	}
}
