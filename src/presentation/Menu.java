package presentation;

import model.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.*;



import javax.swing.*;


public class Menu extends JPanel {
	private JButton persons = new JButton("Persons");
	private JButton accounts = new JButton("Accounts");
	
	private JPanel view;
	private JPanel insert;
	private Bank bank;
	
	public Menu(JPanel view,JPanel insert,Bank bank){
		this.view = view;
		this.bank = bank;
		this.insert = insert;
		
		
		this.setLayout(new GridLayout(1,5));
		this.add(persons);
		this.add(accounts);
		
		persons.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				showPersons();
				
			}
		});
		
		accounts.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {	
				showAccounts();
			}
		});
		this.setPreferredSize(new Dimension(1360,50));
	}

	public void showPersons(){
		List<Person> persons = bank.getPersons();
		view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
		JTable table = ReflectionTable.retrieveTable(persons);
		
		addPersonListener(table,persons);
		
		insert.removeAll();
		
		JTextField name = new JTextField(20);
		JTextField email = new JTextField(30);
		JTextField age = new JTextField(6);
		JButton addPerson = new JButton("Insert");
		
		insert.add(new JLabel("Name :"));
		insert.add(name);
		insert.add(new JLabel("Email :"));
		insert.add(email);
		insert.add(new JLabel("Age :"));
		insert.add(age);
		insert.add(addPerson);
		addPerson.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent i){
				String n = name.getText();
				String e = email.getText();
				int a = new Integer(age.getText());
				
				bank.addPerson(new Person(n,e,a));
				showPersons();
			}
		});
		
		view.removeAll();
		view.add(table);
		view.add(insert);
		view.repaint();
		view.revalidate();
	}
	
	public void showAccounts(){
		List<TableAccount> ac = bank.getAccounts();
		view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
		JTable table = ReflectionTable.retrieveTable(ac);
		
		insert.removeAll();
		
		List<Person> persons = bank.getPersons();
		String[] personsName = new String[persons.size()];
		for(int i = 0 ; i < persons.size() ; i++){
			personsName[i] = persons.get(i).getName();
		}
		String[] types = {"saving account","spending account"};
		JComboBox owner = new JComboBox(personsName);
		JTextField id = new JTextField(30);
		JTextField sold = new JTextField(6);
		JTextField interest = new JTextField(6);
		JComboBox type = new JComboBox(types);
		JButton addAccount = new JButton("Insert");
		
		insert.add(new JLabel("Name :"));
		insert.add(owner);
		insert.add(new JLabel("Id :"));
		insert.add(id);
		insert.add(new JLabel("Sold :"));
		insert.add(sold);
		insert.add(new JLabel("Interest :"));
		insert.add(interest);
		insert.add(new JLabel("Type :"));
		insert.add(type);
		insert.add(addAccount);
		
		addAccount.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String n = owner.getSelectedItem().toString();
				List<Person> persons = bank.getPersons();
				Person myPerson = null;
				for(Person p : persons){
					if(p.getName().equals(n)){
						myPerson = p;
						break;
					}
				}
				
				String i = id.getText();
				float s = new Float(sold.getText());
				float inter = new Float(interest.getText());
				String t = type.getSelectedItem().toString();
				if(t.equals("saving account")){
					SavingAccount ac = new SavingAccount(myPerson,i,s,inter);
					bank.addAccount(myPerson, ac);
				}else{
					SpendingAccount ac = new SpendingAccount(myPerson,i,s);
					bank.addAccount(myPerson, ac);
				}
				showAccounts();
			}
		});
			
		view.removeAll();
		view.add(table);
		view.add(insert);
		view.repaint();
		view.revalidate();
	}

	private void addPersonListener(JTable table,List<Person> persons){
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		    	showPersonsInfo(table,persons);
		    }
		});
	}

	public void showPersonsInfo(JTable table, List<Person> persons){
	    if (table.getSelectedRow() > 0) {
	           String name = table.getValueAt(table.getSelectedRow(), 0).toString();
	           Person person = null;
	           for(Person p : persons){
	        	   if(p.getName().equals(name)){
	        		   person = p;
	        		   break;
	        	   }
	           }
	           final Person x = person;
	           view.removeAll();
	           view.add(new JLabel("Name : " + person.getName()));
	           view.add(new JLabel("Email : " + person.getEmail()));
	           view.add(new JLabel("Age : " + person.getAge()));
	           
	           view.add(new JLabel(" "));
	           view.add(new JLabel(" "));
	           
	           view.add(new JLabel("Accounts: "));
	           view.add(new JLabel(" "));
	           int index=0;
	           List<TableAccount> accounts = new ArrayList<TableAccount>();
	           String[] acId = new String[person.getAccounts().size()];
	           for(Account c : person.getAccounts()){
	        	   	acId[index++] = c.getId();
					String o = c.getOwner();
					String id = c.getId();
					float s = c.getSold();
					float i = c.getInterest();
					String t = null;
					if(c instanceof SavingAccount){
						t = "saving account";
					}else{
						t = "spending account";
					}
					accounts.add(new TableAccount(o,id,s,i,t));
				}
	           
	           JTable table2 = ReflectionTable.retrieveTable(accounts);
	           if(table2 != null){
					view.add(table2);
					table2.setAlignmentX(Component.LEFT_ALIGNMENT);
				}
				else 
					view.add(new JLabel("No accounts for this user!"));
	           view.add(new JLabel(" "));
	           view.add(new JLabel(" "));
	           
	           
	           JPanel withd = new JPanel();
	           withd.add(new JLabel("Make a withdraw:  "));
	          
	          
	           JComboBox selectAccount = new JComboBox(acId);
	           withd.add(selectAccount);
	           withd.add(new JLabel("Amount : "));
	           JTextField amount = new JTextField(10);
	           withd.add(amount);
	           JButton processw = new JButton("process");
	           withd.add(processw);
	           processw.addActionListener(new ActionListener(){
	        	   public void actionPerformed(ActionEvent e){
	        		   String id = selectAccount.getSelectedItem().toString();
	        		   float am = new Float(amount.getText());
	        		   Account ac = null;
	        		   for(Account c : x.getAccounts()){
	        			   if(c.getId().equals(id)){
	        				   ac = c;
	        			   }
	        		   }
	        		   bank.makeWithdraw(ac, am);
	        		   //System.out.println(ac.getOwner());
	        		   showPersonsInfo(table,persons);
	        	   }
	           });
	           withd.setAlignmentX(Component.LEFT_ALIGNMENT);
	           
	           view.add(new JLabel(" "));
	           view.add(new JLabel(" "));
	           
	           JPanel dep = new JPanel();
	           dep.add(new JLabel("Make a deposit:  "));
	          
	          
	           JComboBox selectAccountd = new JComboBox(acId);
	           dep.add(selectAccountd);
	           dep.add(new JLabel("Amount : "));
	           JTextField amountd = new JTextField(10);
	           dep.add(amountd);
	           JButton processd = new JButton("Process");
	           dep.add(processd);
	           processd.addActionListener(new ActionListener(){
	        	   public void actionPerformed(ActionEvent e){
	        		   String id = selectAccount.getSelectedItem().toString();
	        		   float am = new Float(amountd.getText());
	        		   Account ac = null;
	        		   for(Account c : x.getAccounts()){
	        			   if(c.getId().equals(id)){
	        				   ac = c;
	        			   }
	        		   }
	        		   bank.makeDeposit(ac, am);
	        		   //System.out.println(ac.getOwner());
	        		   showPersonsInfo(table,persons);
	        	   }
	           });
	           dep.setAlignmentX(Component.LEFT_ALIGNMENT);
	           
	           JPanel del = new JPanel();
	           del.add(new JLabel("Delete an account : "));
	           JComboBox delac = new JComboBox(acId);
	           JButton delacc = new JButton("Delete account");
	           del.add(delac);
	           del.add(delacc);
	           del.setAlignmentX(Component.LEFT_ALIGNMENT);
	           delacc.addActionListener(new ActionListener(){
	        	   public void actionPerformed(ActionEvent e){
	        		   String id = delac.getSelectedItem().toString();
	        		   Account ac = null;
	        		   for(Account c : x.getAccounts()){
	        			   if(c.getId().equals(id)){
	        				   ac = c;
	        			   }
	        		   }
	        		   bank.removeAccount(x, ac);
	        		   showPersonsInfo(table,persons);
	        	   }
	           });
	           
	           JButton delete = new JButton("Delete person");
	           view.add(withd);
	           view.add(dep);
	           view.add(del);
	           view.add(delete);
	           delete.addActionListener(new ActionListener(){
	        	   public void actionPerformed(ActionEvent i){	   
	        		   bank.removePerson(x);
	        		   showPersons();
	        	   }
	           });
	           
	           view.repaint();
	           view.revalidate();
	        }
	}
}
