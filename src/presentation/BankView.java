package presentation;

import model.*;

import java.awt.*;
import javax.swing.*;

import Serialize.DesiarilizeBank;

import java.awt.event.*;
import java.util.*;



public class BankView extends JFrame {
	
	private JPanel content = new JPanel();
	private JPanel view = new JPanel();
	private JPanel insert = new JPanel();
	private Menu menu;
	private Bank bank;
	
	
	public BankView(Bank bank){
		this.bank = bank;
		menu = new Menu(view,insert,bank);
		initView();
	}
	
	private final void initView(){
		view.setPreferredSize(new Dimension(1360,600));
		view.setLayout(new GridLayout(1,1));
		content.add(menu);
		content.add(view);	
		this.setContentPane(content);
		this.setVisible(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setTitle("Queue simulator");
		this.setResizable(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	
	public static void main(String[] args){
		Bank bank = DesiarilizeBank.deserializeInfo();
		BankView bankv = new BankView(bank);
	}
}
